# Frontend Test

### Test

Membuat Admin Template yang mengikuti guide dari Adobe XD yang sudah disediakan dengan goals:

  - Membiasakan diri dengan XD Implementation
  - GIT Basic Env
  - CSS & JS Simple Test Case
  - Clean Code
  - Good Layering Position (z-index)

#### Instruction :
1. Clone repo ini dan buat branch dengan menggunakan nama "nama-test1".
2. Berikut link untuk XD Reference nya [Frontend Test]
3. Waktu pengerjaan 60 Menit.
4. Jika sudah push di branch masing-masing.

**Good Luck**


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)
   [Frontend Test]: <https://xd.adobe.com/view/380d3970-c3a2-48b2-ac24-f633f58724fc-6d5e/?fullscreen>
